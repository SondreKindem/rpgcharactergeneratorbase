﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase.Thieves
{
    /// <summary>
    /// A thief with a strong backstab
    /// </summary>
    public class Assassin : Thief
    {
        public Assassin(string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating, 20)
        {
        }

        /// <summary>
        /// Backstab a character, dealing 5 extra damage
        /// </summary>
        public void Backstab(Character character)
        {
            Attack(character, BaseDamage + 5);
        }
    }
}
