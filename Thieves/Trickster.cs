﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase.Thieves
{
    /// <summary>
    /// A thief with hiding ability. Low base damage, but increased when hidden
    /// </summary>
    public class Trickster : Thief
    {
        private bool veiled;

        public Trickster(string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating, 15)
        {
        }

        /// <summary>
        /// The trickster vanishes using a smoke bomb.
        /// Gives the dodging status and lets the trickster deal
        /// double damage with garotte
        /// </summary>
        public void SmokeBomb()
        {
            veiled = true;
            Dodge();
        }

        /// <summary>
        /// Strangle an enemy. Damage doubled if hidden in smoke
        /// </summary>
        public void Garrote(Character character)
        {
            int dmg = veiled ? 40 : 20;
            Attack(character, dmg);
        }
    }
}
