﻿using RPGCharacterGeneratorBase;
using RPGCharacterGeneratorBase.Warriors;
using RPGCharacterGeneratorBase.Wizards;
using System;

namespace RPGCharacterGeneratorBaseTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Evoker evoker = new Evoker("evo", 10, 100, 10);
            Brute brute = new Brute("brutus", 10, 10, 10);
            evoker.Attack(brute);
            Console.WriteLine(brute.HP);
        }
    }
}
