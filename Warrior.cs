﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase
{
    /// <summary>
    /// High damage, good versatility
    /// </summary>
    public abstract class Warrior : Character
    {

        public Warrior(string name, int hp, int mana, int armorRating, int baseDamage) : base(name, hp, mana, armorRating, baseDamage)
        {
        }

        /// <summary>
        /// The warrior uses their sword, dealing 10 extra damage
        /// </summary>
        public void Slash(Character character)
        {
            Attack(character, BaseDamage + 10);
        }
    }
}
