﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase.Wizards
{
    /// <summary>
    /// A wizard with strong magic attacks
    /// </summary>
    public class Evoker : Wizard
    {
        public Evoker(string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating, 25)
        {
        }
        
        /// <summary>
        /// Shoot a missile delivering 5 extra damage
        /// </summary>
        /// <returns>true if caster had enough mana, false if not</returns>
        public bool MagicMissile(Character character)
        {
            if(Mana >= 35)
            {
                Attack(character, BaseDamage + 5);
                Mana -= 35;
                return true;
            }
            return false;
        }
    }
}
