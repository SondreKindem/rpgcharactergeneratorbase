﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace RPGCharacterGeneratorBase
{
    public abstract class Character
    {
        private static Random random = new Random();

        public int HP { get; set; }
        public int Mana { get; set; }
        public string Name { get; set; }
        public int BaseDamage { get; }
        public int MaxMana { get; }

        /// <summary>
        /// Armor rating is effectively a damage reduction
        /// </summary>
        public int ArmorRating { get; set; }

        public Character(string name, int hp, int mana, int armorRating, int baseDamage)
        {
            Name = name;
            HP = hp;
            Mana = mana;
            ArmorRating = armorRating;
            BaseDamage = baseDamage;
            MaxMana = mana;
        }

        /// <summary>
        /// Deal damage to another character
        /// </summary>
        /// <param name="character">the character that takes damage</param>
        /// <param name="damage">how much damage should be dealt</param>
        public void Attack(Character character, int damage)
        {
            double modifier = (double)random.Next(1, 10) / 10.0;

            double dmg = (damage * modifier * 2) - ArmorRating;

            if(dmg <= 0)
            {
                Console.WriteLine($"{Name} could not penetrate {character.Name}'s armor");
            }
            else
            {
                character.TakeDamage((int)dmg);
            }
        }

        /// <summary>
        /// Guaranteed hit
        /// </summary>
        public void Attack(Character character)
        {
            double modifier = (double)random.Next(1, 10) / 10.0;

            double dmg = (BaseDamage * (modifier * 2)) - ArmorRating;
            

            character.TakeDamage((int)dmg);
        }

        /// <summary>
        /// Moving restores mana
        /// </summary>
        public virtual void Move()
        {
            if(Mana + 20 > MaxMana)
            {
                Mana = MaxMana;
            }
            else
            {
                Mana += 20;
            }
        }

        /// <summary>
        /// Makes the character lose hp
        /// </summary>
        public virtual void TakeDamage(int damage)
        {
            if(damage > 0)
            {
                Console.WriteLine($"{Name} took {damage} damage");
                HP -= damage;
            }
        }

        public bool IsDead()
        {
            return HP <= 0;
        }
    }
}
