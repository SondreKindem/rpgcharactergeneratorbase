﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase
{
    /// <summary>
    /// Medium damage, low armor, dodge ability
    /// </summary>
    public abstract class Thief : Character
    {
        private bool isDodging = false;

        public Thief(string name, int hp, int mana, int armorRating, int baseDamage) : base(name, hp, mana, armorRating, baseDamage)
        {
        }

        /// <summary>
        /// Dodge the next attack
        /// </summary>
        public void Dodge()
        {
            isDodging = true;
        }

        /// <summary>
        /// The thief stabs a character with their knife
        /// </summary>
        public void Stab(Character character)
        {
            Attack(character, BaseDamage);
        }

        public override void TakeDamage(int damage)
        {
            if (!isDodging)
            {
                HP -= damage;
                isDodging = false;
            }
        }
    }
}
