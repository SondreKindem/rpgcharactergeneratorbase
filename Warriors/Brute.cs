﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase.Warriors
{
    /// <summary>
    /// Medium armor, good damage
    /// </summary>
    public class Brute : Warrior
    {
        public Brute(string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating, 20)
        {
        }

        /// <summary>
        /// The brute smacks a character with the pummel of their sword
        /// </summary>
        public void Pummel(Character character)
        {
            Attack(character, BaseDamage);
        }
    }
}
