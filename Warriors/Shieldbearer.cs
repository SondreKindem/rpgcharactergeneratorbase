﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase.Warriors
{
    /// <summary>
    /// High armor, good damage. (set armor yourself)
    /// </summary>
    public class Shieldbearer : Warrior
    {
        public Shieldbearer(string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating, 15)
        {
        }

        /// <summary>
        /// The shieldbearer slams their shield into another character
        /// </summary>
        public void ShieldBash(Character character)
        {
            Attack(character, BaseDamage);
        }
    }
}
