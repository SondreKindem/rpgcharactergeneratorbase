﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorBase
{
    /// <summary>
    /// Good damage, low hp
    /// </summary>
    public abstract class Wizard : Character
    {
        public Wizard(string name, int hp, int mana, int armorRating, int baseDamage) : base(name, hp, mana, armorRating, baseDamage)
        {
        }

        /// <summary>
        /// Shoot a fireball!
        /// </summary>
        /// <param name="character"></param>
        /// <returns>true if caster had enough mana, false if not</returns>
        public bool Fireball(Character character)
        {
            if(Mana >= 30)
            {
                Attack(character, BaseDamage);
                Mana -= 30;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Wizards restore more mana compared to other characters when moving
        /// </summary>
        public override void Move()
        {
            if (Mana + 30 > MaxMana)
            {
                Mana = MaxMana;
            }
            else
            {
                Mana += 30;
            }
        }
    }
}
